# TezTool


TezTool is a replacement for the alphanet.sh bash scripts, it is a simple GO
app that talks directly to the Docker deamon and implements similar functionality.

# Migrate from alphanet.sh

See: [Migration Guide](./docs/migrate.md)

## Use as Docker Image ( :star: Recommended! :star: )

teztool can be used inside a docker container, to do that, set a bash alias like:

```
alias teztool='docker run -it --rm -v $PWD:/mnt/pwd --network host -e DIND_PWD=$PWD -v /var/run/docker.sock:/var/run/docker.sock registry.gitlab.com/nomadic-labs/teztool:latest'
alias teztool-update='docker pull registry.gitlab.com/nomadic-labs/teztool:latest'
```

## Install / build locally

```
cd src
go build -o teztool *.go 
mv teztool /usr/local/bin/teztool
```

## Quickstart

For more detailed Documentation see:

- [Start | Stop Nodes](./docs/nodes.md)
- [Sandboxes](./docs/sandbox.md)
- [Baking](./docs/baking.md)
- [Baking - Signer Tutorial](./docs/signer.md)


### :globe_with_meridians: Public Networks

Teztool can run one node for every public network (carthagenet/mainnet/...)

#### :rocket: Start a new Node

```
$ teztool create <name> --network mainnet --rpc-port 8732 --history-mode archive
# you can also pass arguments and flags to the tezos-node run command, like:
$ teztool create <name> --network mainnet --rpc-port 8732 --history-mode archive --extra-flags "--singleproccess"

# or just run this and teztool will ask for all input:
$ teztool create

$ teztool <name> start 
```

If you want to start a node from a snapshot, download the file and import it


```
$ teztool <name> snapshot import  <file>
$ teztool <name> start
```

#### :page_facing_up: Config

To change the config file of a network do

```
$ teztool <name> config show  > conf.json
# ( edit your file )
$ teztool <name> config apply conf.json
```

This will automatically restart the node container.


#### :x: Delete Containers

```
$ teztool <name> rm
```

This will delete the running Container for that network, this will NOT remove any Data, the blockchain data,
as well as the (default) tezos-client location are on volumes that this command will not remove

#### :x: Delete Data

```
$ teztool <name> purge
```

This will remove all your data.

#### :white_medium_square: Shell

To use the `tezos-node` and `tezos-client` commands, teztool can open a shell into a temporary container,
that has your current working directory mounted, in this container the node ( if running ) is available via dns name "node"

Example:

```
$ teztool <name> shell

/mounted $ tezos-client -A node rpc get /chains/main/blocks/head/operations
[ [ { "protocol": "PsBabyM1eUXZseaJdmXFApDSBqj8YBfwELoxZHHW77EMcAbbwAS",
      "chain_id": "NetXdQprcVkpaWU",
      "hash": "ooApV3FzukNC12BrmuGVcEchUW6osKab4VA7fp1wFyYKBMRDVB2",
      "branch": "BLnwnJkbi4g8rZ5jEnTPaV73fWExVZvmiv3T3kgCCg8Niwyz8gD",
.....
...

```


### :construction: Sandboxes

Teztool can run one sandbox for every Network, to manage them use the `teztool <name> sandbox` Command.

This command also has flags for the protocol parameters of the Sandbox, run `teztool create --help` to see all of them.


```
$ teztool create <name> [flags] start <port>
# e.g. start with time_between_blocks 1
teztool create mysandbox --network sandbox --rpc-port 58732 --time-between-blocks 1

# or start a sandbox with a baker enbled
teztool create <name> --network sandbox --time-between-blocks 1 --baker-identity bootstrap1

```

Similar to how public networks are handled, Sandboxes can be managed via the following Commands:

```
$ teztool <name> rm <name>
$ teztool <name> purge <name>
$ teztool <name> shell <name>
```


```
$ teztool <name> shell

/mounted $ tezos-client -A node bake for bootstrap2
Injected block BMbVk3Xgz2yk
```


