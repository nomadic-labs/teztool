
TezTool
=======

TezTool is a replacement for the alphanet.sh bash scripts, it is a
simple GO app that talks directly to the Docker deamon and implements
similar functionality.

Welcome to Teztool's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   migrate
   quickstart
   nodes
   baking
   signer
   sandbox

