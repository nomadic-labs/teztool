Teztool: Node Management
========================

Teztool can run Nodes for the following networks:

-  mainnet
-  carthagenet
-  sandbox ( custom network )

Start a node from Scratch
-------------------------

To start a Node and sync the Network from scratch, you can run:

::

   $ teztool create <name>

This will run a node container with the RPC port exposed to 127.0.0.1 on
the port you specified, if you want to access the RPC port from another
machine ( generally not recommended ) you can start it with the
expose-rpc flag:

::

   teztool create <name> --expose-rpc

Start a node from a snapshot
----------------------------

You can start nodes from a snapshot ( in rolling and full history modes
) by downloading a snapshot file and importing it *before* you start the
Node, for Example:

::

   teztool <name> rm # make sure there are no running containers
   teztool <name> purge # make sure data volumes are empty

   # import snapshot
   teztool <name> snapshot import <snapshot.full>

   # start node
   $ teztool <name> start 8732 full

Export a Snapshot
-----------------

Teztool can help you export snapshots with

::

   $ teztool <name> snapshot export <mode> <file>

and with the additional flag ``--with-info`` will write a nice json file
``snapshots.json`` providing additional info about the snapshot file it
just exported, this will look like this:

::

   {
     "Snapshots": {
       "mainnet_rolling": {
         "Filename": "main.rolling",
         "Network": "mainnet",
         "Block": 821229,
         "Blockhash": "BLNVvzpFvD2fndg4D3E6wiiXcRvZwCwCt1G7tdM5A7ygoBEMLSY",
         "Blocktime": "2020-02-12T12:19:51Z"
       }
     },
     "LastChange": "2020-02-27T16:08:42.903669564+01:00"
   }

Stop and Restart
----------------

Teztool runs your nodes in Docker containers, this Command:

::

   $ teztool <name> stop

will stop the Containers but not remove them, so to restart them you
dont need the full ``start`` command, but simply:

::

   $ teztool <name> start

Delete Containers and Data
--------------------------

::

   $ teztool <name> rm 
   # rm ills top and Delete the containers, but
   # keep all the Blockchain and Client Data,
   # because this is stored in seperate docker Volumes


   $ teztool <name> purge
   # will delete the Data Containers
   # this cannot be undone

Backups
-------

Currently, Tezos nodes can only import Snapshot files for full and
rolling history modes, for technical reasons. It is possible to
reconstruct an archive node from a “Full” snapshot, but the process will
take a few days. You can find more information
`here <https://tezos.gitlab.io/user/history_modes.html>`__

If you run an archive Node, and you want to create a backup of it,
teztool can create a .tar.gz file containing your data directory, and
import such a file with

::

   $ teztool <name> backup create <file.tar.gz>

   $ teztool <name> backup import <file.tar.gz>
