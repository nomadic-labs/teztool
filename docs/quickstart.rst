Quickstart
----------

For more detailed Documentation see:

-  `Start \| Stop Nodes <./nodes>`__
-  `Sandboxes <./sandbox>`__
-  `Baking <./baking>`__
-  `Baking - Signer Tutorial <./signer>`__

Public Networks :globe_with_meridians:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Teztool can run one node for every public network
(carthagenet/mainnet/…)

Start a new Node :rocket:
^^^^^^^^^^^^^^^^^^^^^^^^^

::

   $ teztool create <name> --network mainnet --rpc-port 8732 --history-mode archive
   # you can also pass arguments and flags to the tezos-node run command, like:
   $ teztool create <name> --network mainnet --rpc-port 8732 --history-mode archive --extra-flags "--singleproccess"

   # or just run this and teztool will ask for all input:
   $ teztool create

   $ teztool <name> start

If you want to start a node from a snapshot, download the file and
import it

::

   $ teztool <name> snapshot import  <file>
   $ teztool <name> start

Config :page_facing_up:
^^^^^^^^^^^^^^^^^^^^^^^

To change the config file of a network do

::

   $ teztool <name> config show  > conf.json
   # ( edit your file )
   $ teztool <name> config apply conf.json

This will automatically restart the node container.

Delete Containers :x:
^^^^^^^^^^^^^^^^^^^^^

::

   $ teztool <name> rm

This will delete the running Container for that network, this will NOT
remove any Data, the blockchain data, as well as the (default)
tezos-client location are on volumes that this command will not remove

Delete Data :x:
^^^^^^^^^^^^^^^

::

   $ teztool <name> purge

This will remove all your data.

Shell :white_medium_square:
^^^^^^^^^^^^^^^^^^^^^^^^^^^

To use the ``tezos-node`` and ``tezos-client`` commands, teztool can
open a shell into a temporary container, that has your current working
directory mounted, in this container the node ( if running ) is
available via dns name “node”

Example:

::

   $ teztool <name> shell

   /mounted $ tezos-client -A node rpc get /chains/main/blocks/head/operations
   [ [ { "protocol": "PsBabyM1eUXZseaJdmXFApDSBqj8YBfwELoxZHHW77EMcAbbwAS",
         "chain_id": "NetXdQprcVkpaWU",
         "hash": "ooApV3FzukNC12BrmuGVcEchUW6osKab4VA7fp1wFyYKBMRDVB2",
         "branch": "BLnwnJkbi4g8rZ5jEnTPaV73fWExVZvmiv3T3kgCCg8Niwyz8gD",
   .....
   ...

Sandboxes :construction:
~~~~~~~~~~~~~~~~~~~~~~~~

Teztool can run one sandbox for every Network, to manage them use the
``teztool <name> sandbox`` Command.

This command also has flags for the protocol parameters of the Sandbox,
run ``teztool create --help`` to see all of them.

::

   $ teztool create <name> [flags] start <port>
   # e.g. start with time_between_blocks 1
   teztool create mysandbox --network sandbox --rpc-port 58732 --time-between-blocks 1

   # or start a sandbox with a baker enbled
   teztool create <name> --network sandbox --time-between-blocks 1 --baker-identity bootstrap1

Similar to how public networks are handled, Sandboxes can be managed via
the following Commands:

::

   $ teztool <name> rm <name>
   $ teztool <name> purge <name>
   $ teztool <name> shell <name>

::

   $ teztool <name> shell

   /mounted $ tezos-client -A node bake for bootstrap2
   Injected block BMbVk3Xgz2yk

