package cmd

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/nomadic-labs/teztool/internal"
	"gitlab.com/nomadic-labs/teztool/util"
	"go.uber.org/dig"
)

func GetCreateCommand(c *dig.Container) *cobra.Command {
	ExposeRPC := false
	P2pPort := uint(0)
	RPCPort := uint(0)
	net := ""
	config := ""
	sandboxProto := ""
	historyMode := ""
	sandboxParams := internal.ProtoParams{}
	sandboxBakerAddress := ""
	sandboxBlocktime := 5
	extraFlags := ""

	var createNode = &cobra.Command{
		Use:   "create [name] [flags...]",
		Short: "Create and start a new Node",
		Run: func(cmd *cobra.Command, args []string) {
			t := internal.Tezos{}

			t.RunFlags = extraFlags

			t.Network = net
			t.P2pPort = P2pPort
			t.ExposeRPC = ExposeRPC
			t.RpcPort = RPCPort
			t.HistoryMode = historyMode

			if config != "" {
				confBytes, err := getConfigFile(config)
				if err != nil {
					fmt.Println("Error reading custom config file!")
					fmt.Println(err.Error())
					os.Exit(1)
				}
				t.Config = confBytes
			}

			t.Name = GetStringExcept(args, 0, "Name for your Node", []string{})
			if t.Network == "" {
				t.Network = GetArg(args, 100, "Tezos Network", []string{"mainnet", "carthagenet", "sandbox"})
			}
			if t.RpcPort == 0 {
				t.RpcPort = GetNumber(args, 100, "RPC Port")
			}
			if t.Network != "sandbox" && t.HistoryMode == "" {
				t.HistoryMode = GetArg(args, 100, "History Mode", []string{"archive", "full", "rolling"})
			}
			if t.Network == "sandbox" {
				t.HistoryMode = "archive"
				protos := []string{"PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb"} // TODO read from packr files
				if len(protos) != 1 {
					sandboxProto = GetArg(args, 100, "Sandbox Protocol", protos)
				} else {
					sandboxProto = protos[0]
				}

				sandboxParams.TimeBetweenBlocks = strconv.Itoa(sandboxBlocktime)
				t.SandboxProto = sandboxProto
			}

			valdiationErr := validateTezos(t)
			if valdiationErr != nil {
				fmt.Println(valdiationErr)
				os.Exit(1)
			}

			// create node
			time.Sleep(10 * time.Millisecond)
			fmt.Print("\nCreating Tezos Node...\n\n")
			_ = c.Invoke(func(tool *internal.TezTool, files *internal.Files) {
				s := util.GetSpinner("Pulling latest docker image")
				tool.PullImage()
				s.Stop()
				fmt.Println("Pull complete")

				s = util.GetSpinner("Create docker network")
				id, _ := tool.GetNetwork(t.Name)
				t.DockerNetwork = id
				s.Stop()

				s = util.GetSpinner("Create data volume")
				datavol, err := tool.GetDataVolume(t.Name)
				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}
				t.DataVolume = datavol
				s.Stop()

				s = util.GetSpinner("Write tezos-node config file")
				if len(t.Config) == 0 {
					t.Config = files.GetConfig(t.Network)
				}
				err = tool.WriteIntoVolume(t.Config, "", "config.json", t.DataVolume)
				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}
				s.Stop()

				s = util.GetSpinner("Create Client volume")
				clientvol, err := tool.GetClientVolume(t.Name)
				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}
				t.ClientVolume = clientvol
				s.Stop()

				s = util.GetSpinner("Make Sure identity.json exists")
				e := tool.GenerateIdentity(t)
				s.Stop()
				if e != nil {
					fmt.Println(e.Error())
					os.Exit(1)
				}
				s.Stop()

				if t.Network != "sandbox" {
					s = util.GetSpinner("Starting Docker Container")
					id, err = tool.StartTezos(t)
					if err != nil {
						fmt.Println(err.Error())
						os.Exit(1)
					}
					s.Stop()
				} else {
					t.SandboxParams = files.GetSandboxParams(t.SandboxProto, sandboxParams)
					s = util.GetSpinner("Starting Sandbox Container")
					id, err = tool.StartSandbox(t)
					if err != nil {
						fmt.Println(e.Error())
						os.Exit(1)
					}
					s.Stop()

					if sandboxBakerAddress != "" {

						s = util.GetSpinner("Starting Baker for " + sandboxBakerAddress)

						time.Sleep(2 * time.Second) // let node get ready

						tez, err := tool.GetTezos(t.Name)
						if err != nil {
							fmt.Println(err)
							os.Exit(1)
						}
						e := tool.StartSandboxBaker(tez, sandboxProto, sandboxBakerAddress)
						if e != nil {
							fmt.Println(e)
						}
						s.Stop()

						fmt.Println("Everything ready!")
					}

				}

				fmt.Println("Node " + t.Name + " is running!")

			})
		},
	}

	createNode.PersistentFlags().BoolVar(&ExposeRPC, "expose-rpc", false, "make rpc port public")
	createNode.PersistentFlags().UintVarP(&P2pPort, "p2p-port", "u", 0, "custom p2p (udp) port")
	createNode.PersistentFlags().UintVarP(&RPCPort, "rpc-port", "r", 0, "custom rpc (tcp) port")
	createNode.PersistentFlags().StringVarP(&net, "network", "n", "", "network")
	createNode.PersistentFlags().StringVarP(&config, "config", "c", "", "path to custom config file")
	createNode.PersistentFlags().StringVarP(&sandboxProto, "sandboxProto", "s", "genesis", "sandbox protocol")
	createNode.PersistentFlags().StringVarP(&historyMode, "historyMode", "m", "", "archive, full, or rolling")
	createNode.PersistentFlags().StringVarP(&extraFlags, "extra-flags", "e", "", "extra flags")

	// sandbox only
	createNode.PersistentFlags().StringVar(&sandboxBakerAddress, "baker-identity", "", "Auto start a baker for given key alias")
	createNode.PersistentFlags().StringVar(&sandboxProto, "sandbox-proto", "", "Sandbox Protocol")
	createNode.PersistentFlags().IntVar(&sandboxBlocktime, "sandbox-time-between-blocks", 2, "Sandbox minimum time between blocks")
	createNode.PersistentFlags().IntVar(&sandboxParams.BlocksPerCycle, "sandbox-blocks-per-cycle", 16, "Sandbox Protcol Parameter")
	createNode.PersistentFlags().IntVar(&sandboxParams.BlocksPerCommitment, "sandbox-blocks-per-commitment", 8, "Sandbox Protcol Parameter")
	createNode.PersistentFlags().IntVar(&sandboxParams.BlocksPerRollSnapshot, "sandbox-blocks-per-roll-snapshot", 4, "Sandbox Protcol Parameter")
	createNode.PersistentFlags().IntVar(&sandboxParams.BlocksPerVotingPeriod, "sandbox-blocks-per-voting-period", 4, "Sandbox Protcol Parameter")
	createNode.PersistentFlags().IntVar(&sandboxParams.EndorsersPerBlock, "sandbox-endorsers-per-block", 32, "Sandbox Protcol Parameter")
	createNode.PersistentFlags().StringVar(&sandboxParams.HardGasLimitPerOperation, "sandbox-hard-gas-limit-per-operation", "800000", "Sandbox Protcol Parameter")
	createNode.PersistentFlags().StringVar(&sandboxParams.HardGasLimitPerBlock, "sandbox-hard-gas-limit-per-block", "8000000", "Sandbox Protcol Parameter")
	createNode.PersistentFlags().StringVar(&sandboxParams.ProofOfWorkThreshold, "sandbox-proof-of-work-threshold", "-1", "Sandbox Protcol Parameter")
	createNode.PersistentFlags().StringVar(&sandboxParams.TokensPerRoll, "sandbox-tokens-per-roll", "8000000000", "Sandbox Protcol Parameter")
	createNode.PersistentFlags().IntVar(&sandboxParams.MichelsonMaximumTypeSize, "sandbox-michelson-maximum-type-size", 1000, "Sandbox Protcol Parameter")
	createNode.PersistentFlags().StringVar(&sandboxParams.SeedNonceRevelationTip, "sandbox-seed-nonce-revelation-tip", "125000", "Sandbox Protcol Parameter")
	createNode.PersistentFlags().IntVar(&sandboxParams.OriginationSize, "sandbox-origination-size", 257, "Sandbox Protcol Parameter")
	createNode.PersistentFlags().StringVar(&sandboxParams.BlockSecurityDeposit, "sandbox-block-security-deposit", "512000000", "Sandbox Protcol Parameter")
	createNode.PersistentFlags().StringVar(&sandboxParams.EndorsementSecurityDeposit, "sandbox-endorsement-security-deposit", "64000000", "Sandbox Protcol Parameter")
	createNode.PersistentFlags().StringVar(&sandboxParams.CostPerByte, "sandbox-cost-per-byte", "1000", "Sandbox Protcol Parameter")
	createNode.PersistentFlags().StringVar(&sandboxParams.HardStorageLimitPerOperation, "sandbox-hard-storage-limit-per-operation", "60000", "Sandbox Protcol Parameter")
	createNode.PersistentFlags().StringVar(&sandboxParams.TestChainDuration, "sandbox-test-chain-duration", "1966080", "Sandbox Protcol Parameter")
	createNode.PersistentFlags().IntVar(&sandboxParams.QuorumMin, "sandbox-quorum-min", 2000, "Sandbox Protcol Parameter")
	return createNode
}

func validateTezos(t internal.Tezos) error {
	checks := map[string]bool{
		"Node name is not long enough":     len(t.Name) <= 3,
		"RPC Port is invalid":              t.RpcPort <= 0 || t.P2pPort >= 65535,
		"Invalid History Mode":             stringInSlice(t.HistoryMode, []string{"full", "rolling", "archive"}) == false,
		"Invalid Network":                  stringInSlice(t.Network, []string{"mainnet", "carthagenet", "sandbox"}) == false,
		"Invalid History mode for sandbox": t.HistoryMode != "archive" && t.SandboxProto != "",
		"Must specify sandbox protocol":    t.SandboxProto == "" && t.Network == "sandbox",
	}

	for errMsg, check := range checks {
		if check {
			return errors.New("Validation error: " + errMsg)
		}
	}

	return nil
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func getConfigFile(file string) ([]byte, error) {
	if os.Getenv("DIND_PWD") != "" {
		// if i am inside a container, check mounted path
		file = strings.Replace(file, "./", "", 1)
		if file[1] == '/' {
			file = "/mnt/pwd" + file
		} else {
			file = "/mnt/pwd/" + file
		}
	}

	f, err := ioutil.ReadFile(file)
	if err != nil {
		conf := internal.TezConfig{}
		json.Unmarshal(f, &conf)
		if len(conf.RPC.ListenAddrs) >= 1 && conf.RPC.ListenAddrs[0] != "0.0.0.0:8732" {
			fmt.Println("RPC config address must be dafault value \"0.0.0.0:8732\", ")
			fmt.Println("Use the flag `--rpc-port` to change this setting")
			os.Exit(1)
		}
		if conf.P2P.ListenAddr != "" && conf.P2P.ListenAddr != "[::]:9732" {
			fmt.Println("RPC config address must be dafault value \"[::]:9732\", ")
			fmt.Println("Use the flag `--p2p-port` to change this setting")
			os.Exit(1)
		}
	}
	return f, err
}
