package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/nomadic-labs/teztool/internal"
	"go.uber.org/dig"
)

func GetLSCommand(c *dig.Container) *cobra.Command {
	var ls = &cobra.Command{
		Use:   "ls",
		Short: "List all resources managed by teztool",
		Run: func(cmd *cobra.Command, args []string) {

			_ = c.Invoke(func(tool *internal.TezTool, docker *internal.DockerWrapper) {

				vols, _ := docker.FindVolumes()

				fmt.Println("Volumes: ")
				for _, v := range vols {
					fmt.Println("\t * " + v.Name)
				}
				if len(vols) == 0 {
					fmt.Println("\t No Volumes")
				}

				conts, _ := docker.FindContainers()

				fmt.Println("\nContainers:")
				for _, c := range conts {
					fmt.Println("\t* " + c.Names[0][1:] + ", " + c.State)
				}
				if len(conts) == 0 {
					fmt.Println("\t No Containers")
				}

			})

		},
	}

	return ls
}
