package cmd

import (
	"github.com/spf13/cobra"
	"go.uber.org/dig"
)

func GetNodeCommand(c *dig.Container, name string) *cobra.Command {
	var netCmd = &cobra.Command{
		Use:   name,
		Short: "Manage node " + name,
	}

	netCmd.AddCommand(GetStartNodeCommand(c, name))
	netCmd.AddCommand(GetStopNodeCommand(c, name))
	netCmd.AddCommand(GetRMNetworkCommand(c, name))
	netCmd.AddCommand(GetPurgeNetworkCommand(c, name))
	netCmd.AddCommand(GetLogsCommand(c, name))
	netCmd.AddCommand(GetBakeCommand(c, name))
	netCmd.AddCommand(GetSnapshotCommand(c, name))
	netCmd.AddCommand(GetBackupCommand(c, name))
	netCmd.AddCommand(GetShellCommand(c, name))
	netCmd.AddCommand(GetConfigCommand(c, name))
	netCmd.AddCommand(GetUpdateCommand(c, name))
	netCmd.AddCommand(GetWrapperCommand(c, name, "client", "Execute tezos-client commands", "/usr/local/bin/tezos-client -A node"))
	netCmd.AddCommand(GetWrapperCommand(c, name, "exec", "Execute any command", ""))

	return netCmd
}
