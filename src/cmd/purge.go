package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/nomadic-labs/teztool/internal"
	"go.uber.org/dig"
)

func GetPurgeNetworkCommand(c *dig.Container, name string) *cobra.Command {
	var purge = &cobra.Command{
		Use:   "purge",
		Short: "Purge ALL data for " + name,
		Run: func(cmd *cobra.Command, args []string) {

			_ = c.Invoke(func(tool *internal.TezTool, docker *internal.DockerWrapper) {
				tool.PurgeVolumes(name)
			})

		},
	}

	return purge
}
