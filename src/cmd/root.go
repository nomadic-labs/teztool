package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/nomadic-labs/teztool/internal"
	"go.uber.org/dig"
)

func GetRootCmd(c *dig.Container) *cobra.Command {
	var rootCmd = &cobra.Command{
		Use:   "teztool",
		Short: "teztool helps you do things with tezos",
	}

	c.Invoke(func(docker *internal.DockerWrapper) {
		names, err := docker.FindNodes()
		if err != nil {
			fmt.Println("Warning: cant detect running nodes")
		}
		for _, n := range names {
			rootCmd.AddCommand(GetNodeCommand(c, n))
		}
	})

	rootCmd.AddCommand(GetCreateCommand(c))
	rootCmd.AddCommand(GetLSCommand(c))
	rootCmd.AddCommand(GetStatusCommand(c))
	rootCmd.AddCommand(GetUtilCommand(c))
	rootCmd.AddCommand(GetDeleteDataCommand(c))
	rootCmd.AddCommand(GetSignerCommand(c))
	return rootCmd
}
