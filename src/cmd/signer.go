package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/nomadic-labs/teztool/internal"
	"go.uber.org/dig"
)

func GetSignerCommand(c *dig.Container) *cobra.Command {
	var signer = &cobra.Command{
		Use:   "signer",
		Short: "Manage Signer(s)",
	}

	signer.AddCommand(GetSignerCreate(c))

	c.Invoke(func(docker *internal.DockerWrapper) {
		names, err := docker.FindSigners()
		if err != nil {
			fmt.Println("Warning: cant detect running nodes")
		}
		for _, n := range names {
			signer.AddCommand(GetSpecificSignerCommand(c, n))
		}
	})
	return signer
}

func GetSignerCreate(c *dig.Container) *cobra.Command {
	var signer = &cobra.Command{
		Use:   "create [name]",
		Short: "Create a Signer container",
		Run: func(cmd *cobra.Command, args []string) {

			if len(args) != 3 {
				fmt.Println("Usage: create [name] [bind-ip] [port]")
				os.Exit(1)
			}

			_ = c.Invoke(func(tool *internal.TezTool, docker *internal.DockerWrapper) {

				err := tool.CreateSigner(args[0], args[2], args[1])
				if err != nil {
					fmt.Println(err.Error())
					os.Exit(1)
				} else {
					fmt.Println("Signer created!")
					fmt.Println("Configure Addresses with `signer " + args[0] + " shell`,")
					fmt.Println("and then start the signer.")
				}
			})

		},
	}

	return signer
}

func GetSpecificSignerCommand(c *dig.Container, name string) *cobra.Command {
	var signer = &cobra.Command{
		Use:   name,
		Short: "Manage signer " + name,
	}

	signer.AddCommand(GetSignerShell(c, name))
	signer.AddCommand(GetSignerRMCommand(c, name))
	signer.AddCommand(GetSignerStartCmd(c, name))
	signer.AddCommand(GetSignerStopCmd(c, name))
	signer.AddCommand(GetLogsCommand(c, "signer_"+name))

	return signer
}

func GetSignerShell(c *dig.Container, name string) *cobra.Command {
	var signer = &cobra.Command{
		Use:   "shell",
		Short: "Open a shell ( e.g. for tezos-client )",
		Run: func(cmd *cobra.Command, args []string) {

			_ = c.Invoke(func(tool *internal.TezTool, docker *internal.DockerWrapper) {

				fmt.Println("Creating Signer Shell Container...\n")
				vol, _ := tool.GetClientVolume("signer_" + name)
				tool.StartSignerShell(vol)

			})

		},
	}

	return signer
}

func GetSignerStartCmd(c *dig.Container, name string) *cobra.Command {
	var signer = &cobra.Command{
		Use:   "start",
		Short: "Start Signer",
		Run: func(cmd *cobra.Command, args []string) {

			_ = c.Invoke(func(tool *internal.TezTool, docker *internal.DockerWrapper) {

				e := tool.StartSignerContainer(name)
				if e != nil {
					fmt.Println(e)
				}

			})

		},
	}

	return signer
}

func GetSignerStopCmd(c *dig.Container, name string) *cobra.Command {
	var signer = &cobra.Command{
		Use:   "stop",
		Short: "Stop Signer",
		Run: func(cmd *cobra.Command, args []string) {

			_ = c.Invoke(func(tool *internal.TezTool, docker *internal.DockerWrapper) {

				e := tool.StopSignerContainer(name)
				if e != nil {
					fmt.Println(e)
				}

			})

		},
	}

	return signer
}

func GetSignerRMCommand(c *dig.Container, name string) *cobra.Command {
	var signer = &cobra.Command{
		Use:   "rm",
		Short: "Delete Signer",
		Run: func(cmd *cobra.Command, args []string) {

			_ = c.Invoke(func(tool *internal.TezTool, docker *internal.DockerWrapper) {

				e := tool.RemoveSignerContainer(name)
				if e != nil {
					fmt.Println(e)
				}

			})

		},
	}

	return signer
}

/*
func GetSignerStartCommand(c *dig.Container, net string) *cobra.Command {
	var signer = &cobra.Command{
		Use:   "start [bind-ip] [private key]",
		Short: "Start a Signer for a specific address",
		Run: func(cmd *cobra.Command, args []string) {

			if len(args) != 4 {
				fmt.Println("Usage: start [bind-ip] [private key]")
				os.Exit(1)
			}
			_ = c.Invoke(func(tool *internal.TezTool, docker *internal.DockerWrapper) {

				ports := map[string]uint{
					"mainnet":             22000,
					"babylonnet":          22001,
					"carthagenet":         22002,
					"zeronet":             22003,
					"sandbox_babylonnet":  23000,
					"sandbox_carthagenet": 23001,
				}
				tez, err := tool.GetTezos(net)
				tez.Network = net
				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}
				e := tool.StartSigner(tez, args[1], ports[net], args[0])
				if e != nil {
					fmt.Println(e)
				}
				fmt.Println("Signer is running and exposed on Port:", ports[net], "\n\n")
				fmt.Println("On your remote ( baker ) instance run: ")
				fmt.Println("tezos-client import import secret key <keyAlias> tcp://<ip>:<port>/<pkh>")
				fmt.Println("with port:", ports[net])
				fmt.Println("and start/restart the baker")
			})
		},
	}

	return signer
}

func GetSignerStopCommand(c *dig.Container, net string) *cobra.Command {
	var signer = &cobra.Command{
		Use:   "rm",
		Short: "Delete Signer",
		Run: func(cmd *cobra.Command, args []string) {

			_ = c.Invoke(func(tool *internal.TezTool, docker *internal.DockerWrapper) {

				e := tool.RemoveSignerContainer(net)
				if e != nil {
					fmt.Println(e)
				}

			})

		},
	}

	return signer
}

func GetSignerLogsCommand(c *dig.Container, net string) *cobra.Command {
	var logs = &cobra.Command{
		Use:   "logs ",
		Short: "Show signer Logs",
		Run: func(cmd *cobra.Command, args []string) {

			time.Sleep(10 * time.Millisecond)
			e := c.Invoke(func(tool *internal.TezTool, docker *internal.DockerWrapper) {
				cs, _ := docker.FindContainersForNetwork(net)
				for _, c := range cs {
					if _, ok := c.Labels["signer"]; ok {
						stream := tool.LogStream(c.Names[0][1:])
						go func(stream chan string) {
							for {
								log := <-stream
								fmt.Println(log)
							}
						}(stream)
					}
				}

				var signal_channel chan os.Signal
				signal_channel = make(chan os.Signal, 1)
				signal.Notify(signal_channel, os.Interrupt)
				<-signal_channel

			})

			if e != nil {
				panic(e)
			}

		},
	}

	return logs
}

*/
