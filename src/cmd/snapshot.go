package cmd

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/nomadic-labs/teztool/internal"
	"go.uber.org/dig"
)

func GetSnapshotCommand(c *dig.Container, net string) *cobra.Command {
	var snap = &cobra.Command{
		Use:   "snapshot",
		Short: "Import/Export snapshot files",
	}

	snap.AddCommand(GetSnapshotImportCommand(c, net))
	snap.AddCommand(GetSnapshotExportCommand(c, net))
	return snap
}

func GetSnapshotImportCommand(c *dig.Container, name string) *cobra.Command {
	reconstruct := false
	var snap = &cobra.Command{
		Use:   "import [./file]",
		Short: "Import a snapshot file",
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) != 1 {
				fmt.Println("Usage: snapshot import [file]")
				return
			}
			_ = c.Invoke(func(tool *internal.TezTool, docker *internal.DockerWrapper) {

				t := docker.FindNode(name)

				// Create network
				id, net := tool.GetNetwork(t.Name)

				fmt.Println("Using Docker Network: ", id, " ( ID: "+net, ")")
				t.DockerNetwork = id
				// Create Data Volume
				datavol, err := tool.GetDataVolume(t.Name)
				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}
				t.DataVolume = datavol
				fmt.Println("Using Docker volume ( " + t.DataVolume + " ) for Blockchain data")

				// Create Client Volume
				clientvol, err := tool.GetClientVolume(t.Name)
				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}
				t.ClientVolume = clientvol

				fmt.Println("Using Docker volume ( " + t.ClientVolume + " ) for Client data\n\n")

				fmt.Println("Starting helper container....")
				rcarg := ""
				if reconstruct {
					rcarg = " --reconstruct"
				}
				err, code := tool.RunShell(*t, []string{
					"echo 'preparing Data directory....'",
					"sudo rm -rf /home/tezos/.tezos-node/store || true",
					"sudo rm -rf /home/tezos/.tezos-node/context || true",
					"sudo rm -rf /home/tezos/.tezos-node/lock || true",
					"sudo rm -rf /home/tezos/.tezos-node/version || true",
					"sudo chmod -cR 777 /home/tezos/.tezos-node > /dev/null 2>&1",
					"echo 'Starting Import...'",
					"tezos-node config reset --network " + t.Network,
					"cd /mounted && tezos-node snapshot import " + args[0] + rcarg,
				})
				if err != nil {
					fmt.Println(err.Error())
					os.Exit(code)
				}

			})

		},
	}

	snap.PersistentFlags().BoolVar(&reconstruct, "reconstruct", false, "Reconstruct")
	return snap
}

func GetSnapshotExportCommand(c *dig.Container, name string) *cobra.Command {
	withInfo := false
	var snap = &cobra.Command{
		Use:   "export [mode] [./file] [flags]",
		Short: "Export a snapshot file",
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) != 2 {
				fmt.Println("Usage: snapshot export [mode] [./file] [flags]")
				return
			}
			_ = c.Invoke(func(tool *internal.TezTool, docker *internal.DockerWrapper) {

				t := docker.FindNode(name)

				mode := GetArg(args, 0, "Select History Mode", []string{"rolling", "full"})
				t.Name = name
				net_clean := strings.Replace(t.Network, "sandbox_", "", 1)

				block := internal.BlockHeader{}
				if withInfo {
					// gather some info
					nodeId, e := docker.GetContainerId("teztool_" + t.Name)
					info, e := docker.GetContainerInfo(nodeId)
					if e == nil && info.State.Running {
						ip := ""
						for _, n := range info.NetworkSettings.Networks {
							ip = n.IPAddress
							break
						}
						resp, err := http.Get("http://" + ip + ":8732/chains/main/blocks/head~10/header")
						if err == nil {
							body, _ := ioutil.ReadAll(resp.Body)
							json.Unmarshal(body, &block)
						} else {
							fmt.Println("Error getting Block from node: ", err)
							os.Exit(1)
						}

					} else {
						fmt.Println("Could not get Node IP, is it running?")
						os.Exit(1)
					}
				}
				withInfoAppend := ""
				if withInfo {
					withInfoAppend = " --block " + block.Hash
				}

				// Create network
				id, net := tool.GetNetwork(t.Name)

				fmt.Println("Using Docker Network: ", id, " ( ID: "+net, ")")
				t.DockerNetwork = id
				// Create Data Volume
				datavol, err := tool.GetDataVolume(t.Name)
				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}
				t.DataVolume = datavol
				fmt.Println("Using Docker volume ( " + t.DataVolume + " ) for Blockchain data")

				// Create Client Volume
				clientvol, err := tool.GetClientVolume(t.Name)
				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}
				t.ClientVolume = clientvol

				fmt.Println("Using Docker volume ( " + t.ClientVolume + " ) for Client data\n\n")

				fmt.Println("Starting helper container....")
				rollArg := ""
				if mode == "rolling" {
					rollArg = "--rolling "
				}
				err, code := tool.RunShell(*t, []string{
					"echo 'preparing Data directory....'",
					"sudo chmod -cR 777 /home/tezos/.tezos-node > /dev/null 2>&1",
					"echo 'Starting Export'",
					"tezos-node snapshot export --network " + net_clean + " " + rollArg + " /tmp/snap " + withInfoAppend,
					"sudo mv /tmp/snap /mounted/" + args[1],
					"sudo chmod 777 /mounted/" + args[1],
				})

				if err != nil {
					fmt.Println(err.Error())
					os.Exit(code)
				}

				if withInfo {
					info := internal.SnapshotInfos{}
					info.Snapshots = map[string]internal.Snapshot{}
					filename := "./snapshots.json"
					filecontents, _ := ioutil.ReadFile(filename)
					if len(filecontents) != 0 {
						json.Unmarshal(filecontents, &info)
					}
					info.Snapshots[t.Network+"_"+mode] = internal.Snapshot{Network: t.Network, Block: block.Level, Filename: args[1], Blockhash: block.Hash, Blocktime: block.Timestamp}
					info.LastChange = time.Now()
					bytes, _ := json.MarshalIndent(info, "", "  ")
					ioutil.WriteFile(filename, bytes, 0777)
				}

				if err != nil {
					fmt.Println(err.Error())
					os.Exit(1)
				}

			})

		},
	}

	snap.PersistentFlags().BoolVar(&withInfo, "with-info", false, "write json file with extra info")
	return snap
}
