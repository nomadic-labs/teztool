package cmd

import (
	"fmt"
	"os"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/nomadic-labs/teztool/internal"
	"go.uber.org/dig"
)

func GetStartNodeCommand(c *dig.Container, name string) *cobra.Command {

	var startNode = &cobra.Command{
		Use:   "start",
		Short: "Start node",
		Run: func(cmd *cobra.Command, args []string) {
			StartExisting(c, name)
		},
	}
	return startNode
}

// reused in sandbox
func StartExisting(c *dig.Container, name string) {
	// check if existing containers, start and exit if there are
	_ = c.Invoke(func(docker *internal.DockerWrapper) {
		cs, err := docker.FindContainersForName(name)
		if err != nil {
			fmt.Println("Error checking containers, docker running?")
			os.Exit(1)
		}
		startcontainers := func(baker bool) {
			for _, c := range cs {
				_, isbaker := c.Labels["baker"]
				if c.State != "running" && isbaker == baker {
					fmt.Println("starting: ", c.Names[0])
					docker.StartContainer(c.ID)
					time.Sleep(1 * time.Second)
				}
			}
		}
		startcontainers(false) // start all non baker containers
		time.Sleep(1 * time.Second)
		startcontainers(true) // start all baker containers
		if len(cs) != 0 {
			os.Exit(0)
		}
	})
}
