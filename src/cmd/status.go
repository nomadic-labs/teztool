package cmd

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/KyleBanks/dockerstats"
	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
	"gitlab.com/nomadic-labs/teztool/internal"
	"go.uber.org/dig"
)

func GetStatusCommand(c *dig.Container) *cobra.Command {
	format := "nice"
	var ls = &cobra.Command{
		Use:   "status",
		Short: "Show State of all tezos nodes",
		Run: func(cmd *cobra.Command, args []string) {

			_ = c.Invoke(func(tool *internal.TezTool, docker *internal.DockerWrapper) {

				type Node struct {
					Name        string
					ContainerId string
					Version     string
					Network     string
					Mode        string
					Status      string
					RPCExposed  bool
					Blockheight uint64
					BlockHash   string
					IP          string
					Proto       string
					Timestamp   *time.Time
					CPU         string
					Memory      string
				}

				nodes := []Node{}
				conts, _ := docker.FindNodeContainers()

				for _, c := range conts {
					if strings.Contains(c.Names[0], "task") {
						continue
					}
					t := internal.Tezos{}
					json.Unmarshal([]byte(c.Labels["teztool_config"]), &t)
					ips := []string{}
					for _, n := range c.NetworkSettings.Networks {
						ips = append(ips, n.IPAddress)
					}
					nodes = append(nodes,
						Node{
							Name:        c.Labels["node_name"],
							ContainerId: c.ID[:12],
							Network:     c.Labels["network"],
							Mode:        t.HistoryMode,
							Status:      c.State,
							RPCExposed:  t.ExposeRPC,
							IP:          ips[0],
						})
				}

				stats, err := dockerstats.Current()
				if err != nil {
					panic(err)
				}

				for _, s := range stats {
					for i, n := range nodes {
						if n.ContainerId == s.Container {
							nodes[i].Memory = s.Memory.Raw
							nodes[i].CPU = s.CPU
						}
					}
				}

				for i, n := range nodes {
					if n.Status == "running" {
						hash, proto, timestamp, height, err := getNodeBlocks(n.IP)
						if err == nil {
							nodes[i].BlockHash = hash
							nodes[i].Blockheight = uint64(height)
							nodes[i].Proto = proto
							nodes[i].Timestamp = &timestamp
						} else {
							nodes[i].BlockHash = "na"
							nodes[i].Blockheight = 0
							nodes[i].Proto = "na"
							nodes[i].Timestamp = nil
						}
					}
				}

				data := [][]string{}

				boolToStr := func(p bool) string {
					if p {
						return "true"
					}
					return "false"
				}

				for _, n := range nodes {
					lastblock := ""
					if n.Timestamp != nil {
						lastblocks := int(time.Now().Sub(*n.Timestamp).Seconds())
						lastblock = strconv.Itoa(lastblocks) + " seconds ago"
						if lastblocks >= 60*24 {
							lastblock = "!! over a day ago !!"
						}
						if lastblocks <= 60 {
							lastblock = "less than a minute ago"
						}

					}
					bh := strconv.Itoa(int(n.Blockheight))
					data = append(data,
						[]string{
							n.Name,
							n.ContainerId,
							n.Status,
							n.Network,
							n.Mode,
							boolToStr(n.RPCExposed),
							n.CPU,
							n.Memory,
							bh,
							n.BlockHash,
							lastblock,
							n.Proto,
						})
				}

				headers := []string{"Name", "ID", "Status", "Network", "HistoryMode", "RPC Exposed", "CPU", "RAM", "Blockheight", "Blockhash", "Last Block", "Proto"}

				if format == "nice" {
					table := tablewriter.NewWriter(os.Stdout)
					for _, v := range data {
						table.Append(v)
					}
					table.SetHeader(headers)
					table.SetBorder(false)
					fmt.Print("\n")
					table.Render()
					table.SetTablePadding("  ")
					fmt.Print("\n")
				}
				if format == "none" {
					table := tablewriter.NewWriter(os.Stdout)
					for _, v := range data {
						table.Append(v)
					}
					table.SetHeader(headers)
					table.SetBorder(false)
					table.SetCenterSeparator("")
					table.SetAlignment(tablewriter.ALIGN_LEFT)
					table.SetColumnSeparator("\t")
					table.SetHeaderLine(false)
					table.Render()
				}
				if format == "csv" {
					for _, n := range headers {
						fmt.Print(n)
						fmt.Print(";")
					}
					fmt.Print("\n")
					for _, a := range data {
						for _, b := range a {
							fmt.Print(b)
							fmt.Print(";")
						}
						fmt.Print("\n")
					}
				}

			})

		},
	}

	ls.PersistentFlags().StringVarP(&format, "format", "f", "nice", "nice | none | csv")
	return ls
}

func getNodeBlocks(ip string) (hash string, proto string, timestamp time.Time, height uint, err error) {

	var tezClient = &http.Client{
		Timeout: time.Second * 1,
	}

	var res *http.Response
	var body []byte
	var header internal.BlockHeader

	res, err = tezClient.Get("http://" + ip + ":8732/chains/main/blocks/head/header")

	if err == nil {
		body, err = ioutil.ReadAll(res.Body)
	}

	if err == nil {
		err = json.Unmarshal(body, &header)
	}

	if err != nil {
		// initialized with zero values in func signature, except err
		return hash, proto, timestamp, height, err
	}

	hash = header.Hash[:12]
	height = header.Level
	proto = header.Protocol[:12]
	timestamp = header.Timestamp

	return hash, proto, timestamp, height, err
}
