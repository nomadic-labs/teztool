package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/nomadic-labs/teztool/internal"
	"go.uber.org/dig"
)

func GetStopNodeCommand(c *dig.Container, net string) *cobra.Command {
	var stop = &cobra.Command{
		Use:   "stop",
		Short: "Stop a running node",
		Run: func(cmd *cobra.Command, args []string) {
			_ = c.Invoke(func(tool *internal.TezTool, docker *internal.DockerWrapper) {
				tool.StopContainers(net)
			})

		},
	}

	return stop
}
