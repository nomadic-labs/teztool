package cmd

import (
	"fmt"
	"os"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/nomadic-labs/teztool/internal"
	"go.uber.org/dig"
)

func GetUpdateCommand(c *dig.Container, net string) *cobra.Command {
	var rm = &cobra.Command{
		Use:   "update",
		Short: "update Containers to newest version",
		Run: func(cmd *cobra.Command, args []string) {

			_ = c.Invoke(func(docker *internal.DockerWrapper) {
				cs, err := docker.FindContainersForName(net)
				if err != nil {
					fmt.Println("Error checking containers, docker running?")
					os.Exit(1)
				}
				if len(cs) == 0 {
					fmt.Println("No Containers to update for", net)
					return
				}
				updatecontainers := func(baker bool) {
					for _, c := range cs {
						_, isbaker := c.Labels["baker"]
						if isbaker == baker {
							docker.UpdateContainer(c.ID)
						}
					}
				}
				updatecontainers(false) // restart all non baker containers
				time.Sleep(1 * time.Second)
				updatecontainers(true) // restart all baker containers
				if len(cs) != 0 {
					os.Exit(0)
				}
			})

		},
	}

	return rm
}
