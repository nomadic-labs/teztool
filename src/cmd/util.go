package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/nomadic-labs/teztool/internal"
	"go.uber.org/dig"
)

func GetUtilCommand(c *dig.Container) *cobra.Command {
	var util = &cobra.Command{
		Use:   "util",
		Short: "Various little Helper functions",
	}
	util.AddCommand(GetUtilCatCommand(c))
	util.AddCommand(GetCleanCommand(c))
	return util
}

func GetUtilCatCommand(c *dig.Container) *cobra.Command {
	var util = &cobra.Command{
		Use:   "cat [network] [file]",
		Short: "Cat a file from inside the container",
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) != 2 {
				fmt.Println("Usage: cat [network] [file]")
				return
			}
			_ = c.Invoke(func(tool *internal.TezTool, docker *internal.DockerWrapper) {
				res, err := docker.ReadFileInContainer("teztool_"+args[0], args[1])
				if err == nil {
					fmt.Println(string(res))
				} else {
					fmt.Println("No such file found!!!")
				}
			})

		},
	}
	return util
}
