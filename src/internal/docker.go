package internal

import (
	"archive/tar"
	"bufio"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/api/types/mount"
	"github.com/docker/docker/api/types/network"
	"github.com/docker/docker/api/types/volume"
	"github.com/docker/docker/client"
	"github.com/docker/go-connections/nat"
)

type DockerWrapper struct {
	docker *client.Client
}

func NewDockerService() *DockerWrapper {
	cli, err := client.NewEnvClient()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	ds := DockerWrapper{}
	ds.docker = cli
	return &ds
}

func (this *DockerWrapper) PullImage(image string) chan error {
	res := make(chan error)
	go func() {
		_, e := this.PullImageBlocking(image)
		res <- e
	}()
	return res
}

func (this *DockerWrapper) FindNode(name string) *Tezos {
	res := Tezos{}
	cs, _ := this.FindContainers()
	for _, c := range cs {
		if c.Names[0] == "/teztool_"+name {
			json.Unmarshal([]byte(c.Labels["teztool_config"]), &res)
			return &res
		}
	}
	return nil
}

func (this *DockerWrapper) CopyToContainer(container, path string, file io.Reader) error {
	c := context.Background()
	id, err := this.GetContainerId(container)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	return this.docker.CopyToContainer(c, id, path, file, types.CopyToContainerOptions{})
}

func (this *DockerWrapper) ReadFileInContainer(container, path string) ([]byte, error) {
	c := context.Background()
	var res []byte
	id, err := this.GetContainerId(container)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	// this returns a tar stream
	reader, _, err := this.docker.CopyFromContainer(c, id, path)
	if err != nil {
		return res, err
	}
	tr := tar.NewReader(reader)
	for {
		header, _ := tr.Next()
		switch {
		case header == nil:
			continue
		}

		switch header.Typeflag {
		case tar.TypeReg:
			body, _ := ioutil.ReadAll(tr)
			return body, nil
		}
	}
	return res, nil
}

func (this *DockerWrapper) PullImageBlocking(image string) ([]byte, error) {
	ctx := context.Background()
	reader, err := this.docker.ImagePull(ctx, image, types.ImagePullOptions{})
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	return ioutil.ReadAll(reader)
}

func (this *DockerWrapper) Inspect(id string) (types.ContainerJSON, error) {
	ctx := context.Background()
	return this.docker.ContainerInspect(ctx, id)
}

func (this *DockerWrapper) GetLogs(id string, follow bool, tail string) (io.ReadCloser, error) {
	res, e := this.docker.ContainerLogs(context.Background(), id, types.ContainerLogsOptions{
		Follow:     follow,
		ShowStdout: true,
		ShowStderr: true,
		Tail:       tail,
	})

	return res, e
}

func (this *DockerWrapper) ExecRWID(ContainerID string, command []string) (io.Reader, net.Conn, error) {
	ctx := context.Background()

	// get size of the terminal
	cmd := exec.Command("stty", "size")
	cmd.Stdin = os.Stdin
	out, err := cmd.Output()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	sizes := strings.Split(string(out), " ")
	terminalEnvs := []string{
		"COLUMNS=" + sizes[1],
		"LINES=" + sizes[0],
	}

	ex, e := this.docker.ContainerExecCreate(ctx, ContainerID, types.ExecConfig{
		Cmd:          command,
		AttachStdin:  true,
		AttachStderr: true,
		AttachStdout: true,
		Tty:          true,
		Env:          terminalEnvs,
	})
	if e != nil {
		return nil, nil, e
	}
	hr, e := this.docker.ContainerExecAttach(ctx, ex.ID, types.ExecConfig{
		AttachStdin:  true,
		AttachStderr: true,
		AttachStdout: true,
		Tty:          true,
		Env:          terminalEnvs,
	})
	if e != nil {
		return nil, nil, e
	}
	go this.docker.ContainerExecStart(ctx, ex.ID, types.ExecStartCheck{})
	return hr.Reader, hr.Conn, nil
}

func (this *DockerWrapper) GetContainerId(name string) (string, error) {
	ctx := context.Background()
	resp, err := this.docker.ContainerList(ctx, types.ContainerListOptions{
		All: true,
	})
	if err != nil {
		return "", err
	}
	for _, c := range resp {
		for _, n := range c.Names {
			if n == "/"+name {
				return c.ID, nil
			}
		}
	}
	return "", errors.New("No such Container: " + name)
}

func (this *DockerWrapper) FindContainers() ([]types.Container, error) {
	ctx := context.Background()
	res := []types.Container{}
	resp, err := this.docker.ContainerList(ctx, types.ContainerListOptions{
		All: true,
	})
	if err != nil {
		return res, err
	}
	for _, c := range resp {
		if val, ok := c.Labels["created_by"]; ok && val == "teztool" {
			res = append(res, c)
		}
	}
	return res, nil
}

func (this *DockerWrapper) FindNodeContainers() ([]types.Container, error) {
	ctx := context.Background()
	res := []types.Container{}
	resp, err := this.docker.ContainerList(ctx, types.ContainerListOptions{
		All: true,
	})
	if err != nil {
		return res, err
	}
	for _, c := range resp {
		if val, ok := c.Labels["created_by"]; ok && val == "teztool" {
			if val, ok := c.Labels["node"]; ok && val == "true" {
				res = append(res, c)
			}
		}
	}
	return res, nil
}

func (this *DockerWrapper) FindVolumes() ([]*types.Volume, error) {
	ctx := context.Background()
	res := []*types.Volume{}
	resp, err := this.docker.VolumeList(ctx, filters.Args{})
	if err != nil {
		return res, err
	}
	for _, v := range resp.Volumes {
		if val, ok := v.Labels["created_by"]; ok && val == "teztool" {
			res = append(res, v)
		}
	}
	return res, nil
}

func (this *DockerWrapper) FindVolumesForName(name string) ([]*types.Volume, error) {
	res := []*types.Volume{}
	resp, _ := this.FindVolumes()
	for _, c := range resp {
		if val, ok := c.Labels["node_name"]; ok && val == name {
			res = append(res, c)
		}
	}
	return res, nil
}

func (this *DockerWrapper) FindContainersForNetwork(network string) ([]types.Container, error) {
	res := []types.Container{}
	resp, _ := this.FindContainers()
	for _, c := range resp {
		if val, ok := c.Labels["network"]; ok && val == network {
			res = append(res, c)
		}
	}
	return res, nil
}

func (this *DockerWrapper) FindContainersForName(name string) ([]types.Container, error) {
	res := []types.Container{}
	resp, _ := this.FindContainers()
	for _, c := range resp {
		if val, ok := c.Labels["node_name"]; ok && val == name {
			res = append(res, c)
		}
	}
	return res, nil
}

func (this *DockerWrapper) FindRunningNetworks() ([]string, error) {
	res := []string{}
	resp, _ := this.FindContainers()
	for _, c := range resp {
		if c.State == "running" {
			if !contains(res, c.Labels["network"]) {
				res = append(res, c.Labels["network"])
			}
		}
	}
	return res, nil
}

func (this *DockerWrapper) FindRunningNodes() ([]string, error) {
	res := []string{}
	resp, _ := this.FindContainers()
	for _, c := range resp {
		if c.State == "running" {
			if !contains(res, c.Labels["node_name"]) {
				res = append(res, c.Labels["node_name"])
			}
		}
	}
	return res, nil
}

func (this *DockerWrapper) FindNodes() ([]string, error) {
	res := []string{}
	resp, _ := this.FindContainers()
	for _, c := range resp {
		if !contains(res, c.Labels["node_name"]) {
			res = append(res, c.Labels["node_name"])

		}
	}
	return res, nil
}

func (this *DockerWrapper) FindSigners() ([]string, error) {
	res := []string{}
	resp, _ := this.FindContainers()
	for _, c := range resp {
		if !contains(res, c.Labels["teztool_signer"]) {
			res = append(res, c.Labels["teztool_signer_name"])

		}
	}
	return res, nil
}

func (this *DockerWrapper) FindVolumeNetworks() ([]string, error) {
	res := []string{}
	resp, _ := this.FindVolumes()
	for _, c := range resp {
		if !contains(res, c.Labels["network"]) {
			res = append(res, c.Labels["network"])
		}
	}
	return res, nil
}

func (this *DockerWrapper) FindNetworks() ([]string, error) {
	res := []string{}
	resp, _ := this.FindContainers()
	for _, c := range resp {
		if !contains(res, c.Labels["network"]) {
			res = append(res, c.Labels["network"])
		}
	}
	return res, nil
}

func (this *DockerWrapper) CheckNetworkExists(name string) (bool, error) {
	res := false
	nets, e := this.docker.NetworkList(context.Background(), types.NetworkListOptions{})
	for _, n := range nets {
		if strings.ToLower(n.Name) == strings.ToLower(name) || strings.ToLower(n.ID) == strings.ToLower(name) {
			res = true
			break
		}
	}
	return res, e
}

func (this *DockerWrapper) GetVolumes() ([]string, error) {
	res := []string{}
	ctx := context.Background()
	lsResponse, err := this.docker.VolumeList(ctx, filters.Args{})
	if err != nil {
		return res, err
	}
	for _, v := range lsResponse.Volumes {
		res = append(res, v.Name)
	}
	return res, err
}

func (this *DockerWrapper) GetVolume(name, branch string) (string, error) {
	ctx := context.Background()
	lsResponse, err := this.docker.VolumeList(ctx, filters.Args{})
	if err != nil {
		return "", err
	}
	for _, v := range lsResponse.Volumes {
		if v.Name == name {
			return v.Name, nil
		}
	}

	createResp, err := this.docker.VolumeCreate(ctx, volume.VolumesCreateBody{
		Driver:     "",
		DriverOpts: map[string]string{},
		Name:       name,
		Labels:     map[string]string{"created_by": "teztool", "network": branch, "create_time_utc": time.Now().Format("yyyy-MM-dd'T'HH:mm:ssZ")},
	})
	ctx.Done()

	return createResp.Name, err
}

func (this *DockerWrapper) CopyDataVol(src, target string) ([]byte, error) {
	this.PullImageBlocking("docker.io/library/alpine:latest")
	template := GetCopierTemplate(src, target)
	bytes, err := this.RunRM(template)
	return bytes, err
}

func (this *DockerWrapper) Import(src, target string) ([]byte, error) {
	this.PullImageBlocking("docker.io/library/alpine:latest")
	template := GetImporterTemplate(src, target)
	bytes, err := this.RunRM(template)
	return bytes, err
}

func (this *DockerWrapper) GetNetwork(name string) (string, error) {
	ctx := context.Background()
	resp, err := this.docker.NetworkCreate(ctx, name, types.NetworkCreate{
		CheckDuplicate: true,
		Labels: map[string]string{
			"created_by": "teztool",
		},
	})
	ctx.Done()

	nets, _ := this.docker.NetworkList(context.Background(), types.NetworkListOptions{})
	for _, n := range nets {
		if n.Name == strings.ToLower(name) {
			return n.ID, nil
		}
	}

	return resp.ID, err
}

func (this *DockerWrapper) StopContainer(id string) {
	must("stopping Container "+id,
		this.docker.ContainerStop(context.Background(), id, nil))
}

func (this *DockerWrapper) DeleteVolume(id string) {
	e := this.docker.VolumeRemove(context.Background(), id, false)
	must("deleting Volume", e)
}
func (this *DockerWrapper) ReStartContainer(id string) {
	e := this.docker.ContainerRestart(context.Background(), id, nil)
	must("restarting Container", e)
}

func (this *DockerWrapper) StartContainer(id string) {
	e := this.docker.ContainerStart(context.Background(), id, types.ContainerStartOptions{})
	must("starting Container", e)
}

func (this *DockerWrapper) DeleteContainer(id string) {
	e := this.docker.ContainerRemove(context.Background(), id, types.ContainerRemoveOptions{Force: true})
	must("deleting Container", e)
}

func (this *DockerWrapper) DeleteNetwork(netId string) {
	nets, _ := this.docker.NetworkList(context.Background(), types.NetworkListOptions{})
	for _, n := range nets {
		if n.Name == netId || n.ID == netId {
			e := this.docker.NetworkRemove(context.Background(), n.ID)
			must("removing Network", e)
		}
	}
}

func (this *DockerWrapper) UpdateContainer(id string) {
	info, e := this.GetContainerInfo(id)
	wasRunning := info.State.Running
	if e == nil && info.ID == id {
		fmt.Println("Updating Container ", info.Name[1:])
		this.StopContainer(info.ID)
		this.DeleteContainer(info.ID)
		template := DockerTemplate{}
		json.Unmarshal([]byte(info.Config.Labels["teztool_docker_config"]), &template)
		newId, _ := this.ContainerCreate(template)
		if wasRunning {
			this.StartContainer(newId)
		}
	} else {
		must("getting Container Info", e)
	}
}

func (this *DockerWrapper) ContainerCreate(template DockerTemplate) (string, error) {
	tempjson, _ := json.Marshal(template)
	template.Labels["teztool_docker_config"] = string(tempjson)
	ctx := context.Background()
	envs := []string{}
	for k, v := range template.EnvVariables {
		envs = append(envs, k+"="+v)
	}
	mounts := []mount.Mount{}
	for k, v := range template.Mounts {
		mounts = append(mounts,
			mount.Mount{
				Type:   mount.TypeVolume,
				Source: k,
				Target: v,
			})
	}
	for k, v := range template.Binds {
		mounts = append(mounts,
			mount.Mount{
				Type:   mount.TypeBind,
				Source: k,
				Target: v,
			})
	}
	cconfig := container.Config{
		User:         "root",
		Hostname:     "container",
		AttachStdin:  false,
		AttachStdout: false,
		AttachStderr: false,
		Cmd:          template.Command,
		Image:        template.Image,
		Entrypoint:   template.Entrypoint,
		Labels:       template.Labels,
		Env:          envs,
		ExposedPorts: map[nat.Port]struct{}{
			nat.Port("8732/tcp"):  {},
			nat.Port("9732/udp"):  {},
			nat.Port("22000/tcp"): {},
		},
	}
	portBinding := nat.PortMap{}
	for k, v := range template.PortMapping {
		port, err := nat.NewPort("tcp", strconv.Itoa(int(v)))
		if err != nil {
			fmt.Println("Unable to get the port")
			os.Exit(1)
		}
		portBinding[port] = []nat.PortBinding{
			{
				HostIP:   template.ExposeIP,
				HostPort: strconv.Itoa(int(k)),
			},
		}
	}
	for k, v := range template.PortMappingUDP {
		port, err := nat.NewPort("udp", strconv.Itoa(int(v)))
		if err != nil {
			fmt.Println("Unable to get the port")
			os.Exit(1)
		}
		portBinding[port] = []nat.PortBinding{
			{
				HostIP:   "0.0.0.0",
				HostPort: strconv.Itoa(int(k)),
			},
		}
	}
	chostc := container.HostConfig{
		Mounts:       mounts,
		PortBindings: portBinding,
	}
	chostc.Resources.NanoCPUs = int64(template.Resources.CPUCount)
	chostc.Resources.Memory = int64(template.Resources.MemoryMax)
	nc := network.NetworkingConfig{}
	if template.NetworkId != "" {
		nc.EndpointsConfig = map[string]*network.EndpointSettings{}
		nc.EndpointsConfig[template.NetworkId] = &network.EndpointSettings{
			Aliases: template.NetworkAliases,
		}
	}
	resp, err := this.docker.ContainerCreate(ctx, &cconfig, &chostc, &nc, template.ContainerName)
	if err != nil {
		return "", err

	}
	return resp.ID, nil
}

func (this *DockerWrapper) RunRM(template DockerTemplate) ([]byte, error) {
	res := []byte{}
	id, err := this.ContainerCreate(template)
	if err != nil {
		return res, err
	}

	this.StartContainer(id)

	logs, err := this.GetLogs(id, true, "100")
	if err != nil {
		return res, err
	}

	s := bufio.NewScanner(logs)
	for s.Scan() {
		res = append(res, s.Bytes()...)
	}
	this.DeleteContainer(id)
	return res, nil
}

func (this *DockerWrapper) RunRMMany(template DockerTemplate, count int) ([]byte, error) {
	i := 0
	lock := sync.Mutex{}
	done := make(chan struct{})
	ids := []string{}

	for i < count {
		i++
		go func(j int) {
			lock.Lock()
			copy := template
			copy.ContainerName = copy.ContainerName + "_" + strconv.Itoa(j)
			res := []byte{}
			id, _ := this.ContainerCreate(copy)
			this.StartContainer(id)
			ids = append(ids, id)
			lock.Unlock()

			logs, _ := this.GetLogs(id, true, "100")

			s := bufio.NewScanner(logs)
			for s.Scan() {
				res = append(res, s.Bytes()...)
			}
			done <- struct{}{}
		}(i)
	}

	<-done

	for _, n := range ids {
		this.DeleteContainer(n)
	}

	return []byte{}, nil
}

func (this *DockerWrapper) GetContainerInfo(id string) (types.ContainerJSON, error) {
	return this.docker.ContainerInspect(context.Background(), id)
}

func contains(arr []string, str string) bool {
	for _, a := range arr {
		if a == str {
			return true
		}
	}
	return false
}
