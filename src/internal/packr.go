package internal

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"

	"github.com/gobuffalo/packr/v2"
	"github.com/tyler-sommer/stick"
)

func NewPackrBox() *packr.Box {
	box := packr.New("packr", "./../data")
	return box
}

type Files struct {
	box *packr.Box
}

func NewFiles(box *packr.Box) *Files {
	f := Files{}
	f.box = box

	return &f
}

func (*Files) GetAllConfigs() TezConfigs {
	res := []TezConfig{}

	return res
}

func (this *Files) GetConfig(net string) []byte {
	b, _ := this.box.Find("tez_config_" + net + ".json")
	if len(b) >= 1 {
		return b
	} else {
		fmt.Println("Could not get default config for " + net)
		os.Exit(1)
	}
	return []byte{}
}

func (this *Files) GetConfigStruct(net string) *TezConfig {
	res := TezConfig{}
	bytes := this.GetConfig(net)
	json.Unmarshal(bytes, &res)
	return &res
}

func (this *Files) GetDefaultDataPort(net string) int {
	res := 10932
	c := this.GetConfigStruct(net)
	if c != nil {
		port := strings.Replace(c.P2P.ListenAddr, "0.0.0.0", "", -1)
		p, _ := strconv.Atoi(port)
		return p
	}
	return res
}

func (this *Files) GetDefaultRPCPort(net string) int {
	res := 8732
	c := this.GetConfigStruct(net)
	if c != nil {
		port := strings.Replace(c.RPC.ListenAddrs[0], "0.0.0.0", "", -1)
		p, _ := strconv.Atoi(port)
		return p
	}
	return res
}
func (this *Files) GetSandboxParams(proto string, params ProtoParams) string {
	json, _ := this.box.FindString("sandbox_" + proto + "_params.json.twig")
	vars := map[string]stick.Value{}

	vars["bootstrap_accounts"] = stick.Value(params.BootstrapAccounts)
	vars["time_between_blocks"] = stick.Value(params.TimeBetweenBlocks)
	vars["baking_reward_per_endorsement"] = stick.Value(params.BakingRewardPerEndorsement)
	vars["endorsement_reward"] = stick.Value(params.EndorsementReward)
	vars["hard_gas_limit_per_operation"] = stick.Value(params.HardGasLimitPerOperation)
	vars["hard_gas_limit_per_block"] = stick.Value(params.HardGasLimitPerBlock)
	vars["proof_of_work_threshold"] = stick.Value(params.ProofOfWorkThreshold)
	vars["tokens_per_roll"] = stick.Value(params.TokensPerRoll)
	vars["seed_nonce_revelation_tip"] = stick.Value(params.SeedNonceRevelationTip)
	vars["cost_per_byte"] = stick.Value(params.CostPerByte)
	vars["hard_storage_limit_per_operation"] = stick.Value(params.HardStorageLimitPerOperation)
	vars["test_chain_duration"] = stick.Value(params.TestChainDuration)
	vars["block_security_deposit"] = stick.Value(params.BlockSecurityDeposit)
	vars["endorsement_security_deposit"] = stick.Value(params.EndorsementSecurityDeposit)
	vars["delay_per_missing_endorsement"] = stick.Value(params.DelayPerMissingEndorsement)
	vars["blocks_per_cycle"] = stick.Value(params.BlocksPerCycle)
	vars["blocks_per_commitment"] = stick.Value(params.BlocksPerCommitment)
	vars["blocks_per_roll_snapshot"] = stick.Value(params.BlocksPerRollSnapshot)
	vars["blocks_per_voting_period"] = stick.Value(params.BlocksPerVotingPeriod)
	vars["endorsers_per_block"] = stick.Value(params.EndorsersPerBlock)
	vars["michelson_maximum_type_size"] = stick.Value(params.MichelsonMaximumTypeSize)
	vars["origination_size"] = stick.Value(params.OriginationSize)

	env := stick.New(nil)
	env.Filters = map[string]stick.Filter{
		"default": func(ctx stick.Context, val stick.Value, args ...stick.Value) stick.Value {
			notstring := stick.CoerceString(val) == ""
			notnum := stick.CoerceNumber(val) == 0

			if notnum && notstring {
				return args[0]
			}
			return val
		},
	}
	out := bytes.NewBufferString("")
	if err := env.Execute(json, out, vars); err != nil {
		log.Fatal(err)
	}
	return out.String()
}

func (this TezConfigs) NetworkNames() []string {
	n := []string{}
	for _, c := range this {
		n = append(n, c.Network)
	}
	return n
}

type TezConfigs []TezConfig

type TezConfig struct {
	RPC struct {
		ListenAddrs []string `json:"listen-addrs"`
	} `json:"rpc"`
	P2P struct {
		BootstrapPeers []string `json:"bootstrap-peers"`
		ListenAddr     string   `json:"listen-addr"`
	} `json:"p2p"`
	Network string `json:"network"`
}
