package internal

import (
	"bytes"
	"encoding/json"
)

func GetSandboxProtocol(net string) string {
	protos := map[string]string{
		"carthagenet": "PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb",
		"babylonnet":  "PsBabyM1eUXZseaJdmXFApDSBqj8YBfwELoxZHHW77EMcAbbwAS",
	}
	return protos[net]
}

// This combines the params for multiple Protocols,
// if one does not use all, leave the field empty and json marshall will ignore it
type ProtoParams struct {
	BootstrapAccounts            [][]string `json:"bootstrap_accounts,omitempty"`
	TimeBetweenBlocks            string     `json:"time_between_blocks,omitempty"`
	BakingRewardPerEndorsement   []string   `json:"baking_reward_per_endorsement,omitempty"`
	EndorsementReward            []string   `json:"endorsement_reward,omitempty"`
	HardGasLimitPerOperation     string     `json:"hard_gas_limit_per_operation,omitempty"`
	HardGasLimitPerBlock         string     `json:"hard_gas_limit_per_block,omitempty"`
	ProofOfWorkThreshold         string     `json:"proof_of_work_threshold,omitempty"`
	TokensPerRoll                string     `json:"tokens_per_roll,omitempty"`
	SeedNonceRevelationTip       string     `json:"seed_nonce_revelation_tip,omitempty"`
	CostPerByte                  string     `json:"cost_per_byte,omitempty"`
	HardStorageLimitPerOperation string     `json:"hard_storage_limit_per_operation,omitempty"`
	TestChainDuration            string     `json:"test_chain_duration,omitempty"`
	BlockSecurityDeposit         string     `json:"block_security_deposit,omitempty"`
	EndorsementSecurityDeposit   string     `json:"endorsement_security_deposit,omitempty"`
	DelayPerMissingEndorsement   string     `json:"delay_per_missing_endorsement,omitempty"`
	PreservedCycles              int        `json:"preserved_cycles,omitempty"`
	BlocksPerCycle               int        `json:"blocks_per_cycle,omitempty"`
	BlocksPerCommitment          int        `json:"blocks_per_commitment,omitempty"`
	BlocksPerRollSnapshot        int        `json:"blocks_per_roll_snapshot,omitempty"`
	BlocksPerVotingPeriod        int        `json:"blocks_per_voting_period,omitempty"`
	EndorsersPerBlock            int        `json:"endorsers_per_block,omitempty"`
	MichelsonMaximumTypeSize     int        `json:"michelson_maximum_type_size,omitempty"`
	OriginationSize              int        `json:"origination_size,omitempty"`
	QuorumMin                    int        `json:"quorum_min,omitempty"`
	QuorumMax                    int        `json:"quorum_max,omitempty"`
	MinProposalQuorum            int        `json:"min_proposal_quorum,omitempty"`
	InitialEndorsers             int        `json:"initial_endorsers,omitempty"`
	EndorsementRewardString      *string    `json:"endorsement_reward_DUPLICATE,omitempty"`
	BlockReward                  *string    `json:"block_reward,omitempty"`
}

func (this *ProtoParams) GetJson() []byte {
	res, _ := json.Marshal(this)
	res = bytes.Replace(res, []byte("_DUPLICATE"), []byte{}, -1)
	return res
}
