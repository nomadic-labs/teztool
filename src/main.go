package main

import (
	"gitlab.com/nomadic-labs/teztool/cmd"
	"gitlab.com/nomadic-labs/teztool/internal"
	"go.uber.org/dig"
)

func GetDig() *dig.Container {
	c := dig.New()
	_ = c.Provide(internal.NewTezTool)
	_ = c.Provide(internal.NewDockerService)
	_ = c.Provide(internal.NewPackrBox)
	_ = c.Provide(internal.NewFiles)
	return c
}

func main() {

	c := GetDig()
	root := cmd.GetRootCmd(c)

	_ = root.Execute()

}
